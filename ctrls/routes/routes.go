package routes

import (
	"net/http"

	"bitbucket.org/rmanolis/cryptobureau/ctrls"
	"bitbucket.org/rmanolis/cryptobureau/utils"
	"github.com/gorilla/mux"
)

func PrototypeFile(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "ui/index.html")
}

func SetRouter(router *mux.Router) {
	router.HandleFunc("/", PrototypeFile)
	auth := router.PathPrefix("/auth").Subrouter()
	auth.HandleFunc("/register", ctrls.Register).Methods("POST")
	auth.HandleFunc("/login", ctrls.RequestLogin).Methods("GET")
	auth.HandleFunc("/login/{id}", ctrls.Login).Methods("POST")
	auth.HandleFunc("/user", ctrls.CurrentUser).Methods("GET")
	auth.HandleFunc("/logout", utils.AuthUserHandler(ctrls.Logout)).Methods("POST")

}
