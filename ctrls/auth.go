package ctrls

import (
	"encoding/json"
	"log"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/rmanolis/cryptobureau/models"
	"bitbucket.org/rmanolis/cryptobureau/utils"
	"github.com/gorilla/mux"
)

func RequestLogin(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	login := new(models.Login)
	err := login.Insert(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	b, err := json.Marshal(login)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

type LoginCredentials struct {
	Username  string
	Signature string
}

func Login(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	db := utils.GetDB(r)
	decoder := json.NewDecoder(r.Body)
	credentials := new(LoginCredentials)
	err := decoder.Decode(&credentials)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, "Wrong password or email", http.StatusNotAcceptable)
		return
	}
	log.Println(credentials)
	user, err := models.GetUserByName(db, credentials.Username)
	if err != nil {
		http.Error(w, "did not find user", http.StatusNotFound)
		return
	}
	login, err := models.GetLoginById(db, bson.ObjectIdHex(id))
	if err != nil {
		http.Error(w, "did not find login", http.StatusNotFound)
		return
	}
	if login.IsAccepted {
		http.Error(w, "not found", http.StatusNotFound)
		return
	}

	login.UserId = user.Id
	login.Signature = credentials.Signature
	login.IP = r.RemoteAddr
	verified, err := login.Verify(user)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	if !verified {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	utils.SetSession(w, r, "user_id", user.Id.Hex())
	if user.IsAdmin {
		utils.SetSession(w, r, "admin_id", user.Id.Hex())
	}
	login.IsAccepted = true
	login.Update(db)
	b, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)

}

type RegisterCredentials struct {
	Username  string
	PublicKey string
	Email     string
}

func Register(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	decoder := json.NewDecoder(r.Body)
	credentials := new(RegisterCredentials)
	err := decoder.Decode(&credentials)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	if credentials.Email == "" {
		http.Error(w, "email is empty", http.StatusNotAcceptable)
		return
	}
	user := new(models.User)
	user.Username = credentials.Username
	user.PublicKey = credentials.PublicKey
	user.Email = credentials.Email
	user.IsAdmin = true
	err = user.Insert(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	b, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	utils.DeleteCookies(w, r)
	http.Redirect(w, r, "/", 302)
}

func CurrentUser(w http.ResponseWriter, r *http.Request) {
	bid, err := utils.GetUserId(r)
	if err != nil || bid == nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	db := utils.GetDB(r)
	user, err := models.GetUserById(db, *bid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	b, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)

}
