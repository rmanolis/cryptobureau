package main

import (
	"crypto/elliptic"

	"encoding/gob"

	"bitbucket.org/rmanolis/cryptobureau/utils/ecdsa"
	"github.com/gopherjs/gopherjs/js"
)

func main() {
	gob.Register(elliptic.P256())
	gob.Register(ecdsa.Signature{})
	js.Global.Set("ecdsa", map[string]interface{}{
		"New": New,
	})

}

type Ecdsa struct {
}

func New() *js.Object {
	return js.MakeWrapper(&Ecdsa{})
}

func (p *Ecdsa) GenerateKeys(curve string) (*ecdsa.Keys, error) {
	return ecdsa.GenerateKeys(curve)
}

func (p *Ecdsa) Verify(pub []byte, str string, sigb []byte) (bool, error) {
	return ecdsa.Verify(pub, str, sigb)
}

func (p *Ecdsa) Sign(text string, prv []byte) ([]byte, error) {
	return ecdsa.Sign(text, prv)
}
