package ecdsa

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"encoding/gob"
	"encoding/pem"
	"errors"
	"log"
	"math/big"
	"strings"
)

const (
	P521 = "P521"
	P384 = "P384"
	P256 = "P256"
)

// Choose P521, P384, P256
func chooseCurve(cur string) elliptic.Curve {
	if cur == P521 {
		return elliptic.P521()
	} else if cur == P384 {
		return elliptic.P384()
	} else {
		return elliptic.P256()
	}
}

type Signature struct {
	R, S *big.Int
}

type signatureJson struct {
	r, s []byte
}

func (s *Signature) MarshalText() ([]byte, error) {
	var gb bytes.Buffer
	genc := gob.NewEncoder(&gb)
	err := genc.Encode(s)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	b := pem.Block{
		Type:  "DSA SIGNATURE",
		Bytes: gb.Bytes(),
	}
	return pem.EncodeToMemory(&b), nil
}

func (s *Signature) UnmarshalText(text []byte) error {
	for {
		bl, r := pem.Decode(text)
		if bl != nil {

			if bl.Type == "DSA SIGNATURE" {
				b := bytes.NewBuffer(bl.Bytes)
				gdec := gob.NewDecoder(b)
				err := gdec.Decode(s)
				if err != nil {
					log.Println(err.Error())
					return err
				}
				return nil
			}
		} else {
			break
		}
		text = r
	}
	return errors.New("Signature block not found")
}

// PublicKey represents an ECDSA public key.
type PublicKey struct {
	X, Y      *big.Int
	CurveName string
}

func (pk *PublicKey) fromPublicKey(k *ecdsa.PublicKey, curve_name string) {
	pk.X = k.X
	pk.Y = k.Y
	pk.CurveName = curve_name
}

func (pubk *PublicKey) toPublicKey(curve elliptic.Curve) *ecdsa.PublicKey {
	pk := new(ecdsa.PublicKey)
	pk.Curve = curve
	pk.X = pubk.X
	pk.Y = pubk.Y
	return pk
}

func (p *PublicKey) MarshalText() (text []byte, err error) {
	curve := chooseCurve(p.CurveName)
	pub := ecdsa.PublicKey{Curve: curve, X: p.X, Y: p.Y}
	x, err := x509.MarshalPKIXPublicKey(&pub)
	if err != nil {
		return nil, err
	}
	b := pem.Block{
		Type:  p.CurveName + " DSA PUBLIC KEY",
		Bytes: x,
	}
	return pem.EncodeToMemory(&b), nil
}

func (p *PublicKey) UnmarshalText(text []byte) error {
	for {
		bl, r := pem.Decode(text)
		if bl != nil {
			bl_name := strings.Split(bl.Type, " ")
			if bl_name[1] == "DSA" && bl_name[2] == "PUBLIC" && bl_name[3] == "KEY" {
				k, err := x509.ParsePKIXPublicKey(bl.Bytes)
				if err != nil {
					return err
				}
				p.X = k.(*ecdsa.PublicKey).X
				p.Y = k.(*ecdsa.PublicKey).Y
				p.CurveName = bl_name[0]
				return nil
			}
		} else {
			break
		}
		text = r
	}
	return errors.New("Public key block not found")
}

func (p *PublicKey) IsZero() bool {
	return (p.X == nil || p.Y == nil || p.X.Int64() == 0 || p.Y.Int64() == 0)
}

func (p *PublicKey) Verify(data []byte, sign *Signature) bool {
	if p.IsZero() {
		return false
	}
	if sign == nil || sign.R == nil || sign.S == nil {
		return false
	}
	if p.X == nil || p.Y == nil {
		return false
	}
	curve := chooseCurve(p.CurveName)
	key := ecdsa.PublicKey{
		Curve: curve,
		X:     p.X,
		Y:     p.Y,
	}
	hash := sha256.Sum256(data)
	return ecdsa.Verify(&key, hash[:], sign.R, sign.S)
}

// PrivateKey represents an ECDSA private key.
type PrivateKey struct {
	PublicKey
	D *big.Int
}

func (p *PrivateKey) Sign(data []byte) *Signature {
	curve := chooseCurve(p.PublicKey.CurveName)
	key := ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{Curve: curve, X: p.X, Y: p.Y},
		D:         p.D,
	}
	hash := sha256.Sum256(data)
	r, s, err := ecdsa.Sign(rand.Reader, &key, hash[:])
	if err == nil {
		return &Signature{R: r, S: s}
	}
	return nil
}

func (p *PrivateKey) MarshalText() (text []byte, err error) {
	curve := chooseCurve(p.PublicKey.CurveName)
	pri := ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{Curve: curve, X: p.X, Y: p.Y},
		D:         p.D,
	}
	x, err := x509.MarshalECPrivateKey(&pri)
	if err != nil {
		return nil, err
	}
	b := pem.Block{
		Type:  p.CurveName + " DSA PRIVATE KEY",
		Bytes: x,
	}
	return pem.EncodeToMemory(&b), nil
}

func (p *PrivateKey) UnmarshalText(text []byte) error {
	for {
		bl, r := pem.Decode(text)
		if bl != nil {
			bl_name := strings.Split(bl.Type, " ")
			if bl_name[1] == "DSA" && bl_name[2] == "PRIVATE" && bl_name[3] == "KEY" {
				k, err := x509.ParseECPrivateKey(bl.Bytes)
				if err != nil {
					return err
				}
				p.D = k.D
				p.PublicKey.X = k.PublicKey.X
				p.PublicKey.Y = k.PublicKey.Y
				p.PublicKey.CurveName = bl_name[0]
				return nil
			}
		} else {
			break
		}
		text = r
	}
	return errors.New("Private key block not found")
}

func (pk *PrivateKey) fromPrivateKey(k *ecdsa.PrivateKey, curve_name string) {
	pubk := new(PublicKey)
	pubk.fromPublicKey(&k.PublicKey, curve_name)
	pk.PublicKey = *pubk
	pk.D = k.D
}

func (prik *PrivateKey) toPrivateKey(curve elliptic.Curve) *ecdsa.PrivateKey {
	pk := new(ecdsa.PrivateKey)
	pk.Curve = curve
	pk.X = prik.X
	pk.Y = prik.Y
	pk.D = prik.D
	return pk
}
