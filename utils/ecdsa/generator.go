package ecdsa

import (
	"crypto/ecdsa"
	"crypto/rand"
	"log"
)

type Keys struct {
	PublicKey  []byte
	PrivateKey []byte
}

func GenerateKeys(curve_name string) (*Keys, error) {
	pubkeyCurve := chooseCurve(curve_name)

	privatekey, err := ecdsa.GenerateKey(pubkeyCurve, rand.Reader)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	pk := new(PrivateKey)
	pk.fromPrivateKey(privatekey, curve_name)
	pri_bytes, err := pk.MarshalText()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	pubk := new(PublicKey)
	pubk.fromPublicKey(&privatekey.PublicKey, curve_name)
	pub_bytes, err := pubk.MarshalText()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	keys := new(Keys)
	keys.PrivateKey = pri_bytes
	keys.PublicKey = pub_bytes
	return keys, nil
}
