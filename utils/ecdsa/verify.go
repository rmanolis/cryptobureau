package ecdsa

import "log"

func Verify(pub []byte, str string, sigb []byte) (bool, error) {
	pubk := new(PublicKey)
	err := pubk.UnmarshalText(pub)
	if err != nil {
		log.Println(err.Error())
		return false, err
	}

	sig := new(Signature)
	err = sig.UnmarshalText(sigb)
	if err != nil {
		log.Println(err.Error())
		return false, err
	}

	v := pubk.Verify([]byte(str), sig)
	return v, nil
}
