package ecdsa

func Sign(str string, private []byte) ([]byte, error) {
	prik := new(PrivateKey)
	err := prik.UnmarshalText(private)
	if err != nil {
		return nil, err
	}
	signature := prik.Sign([]byte(str))
	return signature.MarshalText()
}
