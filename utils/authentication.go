package utils

import (
	"errors"
	"math/rand"
	"net/http"
	"time"

	"bitbucket.org/rmanolis/cryptobureau/confs"
	"github.com/gorilla/sessions"
	"gopkg.in/mgo.v2/bson"
)

var store = sessions.NewCookieStore([]byte("secret"))

func GetUserId(r *http.Request) (*bson.ObjectId, error) {
	str, err := GetSession(r, "user_id")
	if err != nil {
		return nil, err
	}
	if !bson.IsObjectIdHex(str) {
		return nil, errors.New("not an object id")
	}
	id := bson.ObjectIdHex(str)
	return &id, nil
}

func GetAdminId(r *http.Request) (*bson.ObjectId, error) {
	str, err := GetSession(r, "admin_id")
	if err != nil {
		return nil, err
	}
	if !bson.IsObjectIdHex(str) {
		return nil, errors.New("not an object id")
	}
	id := bson.ObjectIdHex(str)
	return &id, nil
}

func SetSession(w http.ResponseWriter, r *http.Request, name, value string) {
	session, err := store.Get(r, "session")
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}

	session.Values[name] = value
	session.Save(r, w)
}

func GetSession(r *http.Request, name string) (string, error) {
	session, err := store.Get(r, "session")
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}
	cookie, ok := session.Values[name]
	if !ok {
		return "", errors.New("no cookie")
	}
	s, ok := cookie.(string)
	if !ok {
		return "", errors.New("no cookie")
	}
	return s, nil
}
func AuthUserMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	_, err := GetUserId(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	} else {
		next(w, r)
	}
}

func AuthUserHandler(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, err := GetUserId(r)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		} else {
			next(w, r)
		}
	}
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandSeq(n int) string {
	rand.Seed(time.Now().Unix())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func DeleteCookies(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session")
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}

	//session.Options.MaxAge = -1

	delete(session.Values, "user_id")
	delete(session.Values, "admin_id")
	session.Save(r, w)

}
