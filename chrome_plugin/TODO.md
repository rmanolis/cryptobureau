- Methods for KeysModel
  x Generate key and add  URL, Username and Email  
  x select and show in wide textare the public key
  - When opens select the first key and edit
  - When pressing "Add" button, remove the first key and show empty inputs with generate button
  - (For later on) Only private keys are encrypted

- Component that lists names of keys 
  - list all names and filter by search
  - current url or all
    - if current url then show only names else show names with the urls
  - select uuid 

- Sign
  - lists names (by component)
  - big size textarea for the textToSign (#cb-text-sign)
  - medium size textarea for the key
  - button sign
  - if there is #cb-signature-set then set there the signature

- Verify
 - list names (and have checkbox to choose between your keys or your friends)
 - big size textarea for the textToVerify
 - medium size textarea for the key
 - button verify
 - if there is #cb-text-verify , #cb-public-key and #cb-signature-get , then inform plugin

- Friends
 - add friend username , url , email and public key
 - list names
 - select and show in wide textarea the public key

