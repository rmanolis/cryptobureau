import React, {Component} from 'react';
import {observer} from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';

@observer
class KeysCtrl extends Component {
  constructor(props){
    super(props); 
    props.keysModel.getAll();
    this.state = { 
      username:"",
      email:"",
      url:"",
      curve:"",
    }

  }

  componentWillMount(){
    const newState = update(this.state,{text:{$set:this.props.domModel.textToSign}})
    this.setState(newState);
  }

  selectKey(key){
    console.log(key.id);
    this.props.keysModel.selectKey(key.id);
  }

  deleteKey(id){
    console.log(id);
    this.props.keysModel.deleteKey(id);    
  }


  listKeys(){
    return (
      <div>
        { this.props.keysModel.keys.map(x => 
                                        <div className="row" key={x.id} >
                                            <div className="col-sm-8">
                                              <span onClick={() => this.selectKey(x)}>- { x.username }</span>
                                            </div>
                                            <div className="col-sm-1">
                                              <button onClick={ () => this.deleteKey(x.id)}>x</button>
                                            </div>
                                          </div>
                                        ) }
                                      </div>
    )
  }

  signText =()=>{
    this.props.keysModel.signText(this.state.text).then(function(signature){
      const newState = update(this.state,{result:{$set:signature.toString()}})
      this.setState(newState);
    }.bind(this))
  }

  showSelectedKey(){
    const key = this.props.keysModel.selectedKey;
    if(key.id){
      return (
        <div>
          <div> <label>Username: </label>
            <span>{key.username} </span>
          </div>
          <div> <label>Email: </label>
            <span>{key.email}</span>
          </div>
          <div> <label>URL: </label>
            <span>{key.url} </span>
          </div>                  
          <div> <label>Public key: </label>
            <div>
              <textarea style={{height:150, width:380}} value={key.publicKey.toString()}></textarea>
            </div>
          </div>
          <div> <label>Private key: </label>
            <div>
              <textarea style={{height:150, width:380}} value={key.privateKey.toString()}></textarea>
            </div>
          </div>
        </div>
      )
    }else{
      return (<div/>)
    }
  }

  render() {
    return (
      <div className="container-fluid">
        <span> Keys </span>
        <div className="row">
          <div className="col-sm-3">
            { this.listKeys() }
          </div>
          <div className="col-sm-6">
            { this.showSelectedKey() }            
          </div>
          <div className="col-sm-2">
            <div> <label>Username: </label>
              <input type="text" valueLink={this.linkState('username')} />
            </div>
            <br/>
            <div> <label>Email: </label>
              <input type="text" valueLink={this.linkState('email')} />
            </div>
            <br/>
            <div> <label>URL: </label>
              <input type="text" valueLink={this.linkState('url')} />
            </div>
            <br/>
            <div> <label>Curve: </label>
              <select valueLink={this.linkState('curve')}>
                <option value="P256">P256</option>
                <option value="P384">P384</option>
                <option value="P521">P521</option>
              </select>
            </div>
            <br/>
            <button  onClick={() => {
              if(this.state.username){
                this.props.keysModel.generateKey(this.state.username, this.state.url, 
                                                 this.state.email, this.state.curve)
              }else{
                alert("add username");
              }
            } }>generate</button>  
        </div>
      </div>
    </div>
    );
  }
}

reactMixin(KeysCtrl.prototype, LinkedStateMixin)

export default KeysCtrl;

