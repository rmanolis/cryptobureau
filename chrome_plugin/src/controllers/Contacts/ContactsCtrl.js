import React, {Component} from 'react';
import {observer} from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';

@observer
class ContactsCtrl extends Component {
  constructor(props){
    super(props); 
    this.state = {
      username: "",
      url: "",
      email: "",
      publicKey: ""
    }
  }

  componentWillMount(){
    this.props.contactsModel.getAll();
  }

  listContacts(){
    const { selectContact, deleteContact } = this.props.contactsModel;
    return (
      <div>
        { this.props.contactsModel.contacts.map(x => 
                                            <div className="row" key={x.id} >
                                              <div className="col-sm-8">
                                                <span onClick={() => selectContact(x.id)}>- { x.username }</span>
                                              </div>
                                              <div className="col-sm-1">
                                                <button onClick={ () => deleteContact(x.id)}>x</button>
                                              </div>
                                            </div>
                                            ) }
                                          </div>
    )
  }

  addContact(){
    return(
      <div>
        <div> <label>Username: </label>
          <input type="text" valueLink={this.linkState('username')} />
        </div>
        <br/>
        <div> <label>Email: </label>
          <input type="text" valueLink={this.linkState('email')} />
        </div>
        <br/>
        <div> <label>URL: </label>
          <input type="text" valueLink={this.linkState('url')} />
        </div>
        <div> <label>Public key: </label>
          <textarea style={{height:150, width:150}} type="text" valueLink={this.linkState('publicKey')} />
        </div>

        <br/>
        <button  onClick={() => {
          const {username, url, email, publicKey } = this.state;
          if(username){
            this.props.contactsModel.addContact(username,url, 
                                                email, publicKey)
          }else{
            alert("add username");
          }
        } }>Add</button>  
    </div>
    )
  }

  showSelectedContact(){
    const contact = this.props.contactsModel.selectedContact;
    if(contact.id){
      return (
        <div>
          <div> <label>Username: </label>
            <span>{contact.username} </span>
          </div>
          <div> <label>Email: </label>
            <span>{contact.email}</span>
          </div>
          <div> <label>URL: </label>
            <span>{contact.url} </span>
          </div>                  
          <div> <label>Public key: </label>
            <div>
              <textarea style={{height:150, width:380}} value={contact.publicKey.toString()}></textarea>
            </div>
          </div>
        </div>
      )
    }else{
      return (<div/>)
    }
  }



  render() {
    return (
      <div>
        <span> Contacts </span>
        <div className="row">
          <div className="col-sm-3">
            { this.listContacts() }
          </div>
          <div className="col-sm-6">
            { this.showSelectedContact() }
          </div>
          <div className="col-sm-3" >
            { this.addContact() }
          </div>
        </div>
      </div>
    );
  }

};

reactMixin(ContactsCtrl.prototype, LinkedStateMixin)


export default ContactsCtrl;


