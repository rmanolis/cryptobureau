import React, {Component} from 'react';
import {observer} from 'mobx-react';
import ListKeysCmp from '../../components/ListKeys/ListKeysCmp.js'
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';


@observer
class SignCtrl extends Component {
  constructor(props){
    super(props);  
    this.state = {
      text:"",
      signature:"",
    }
  }
  componentWillMount(){
    this.props.keysModel.getAll();  
    const newState = update(this.state,{text:{$set:this.props.domModel.textToSign}})
    this.setState(newState);
  }
  
  
  signText =()=>{
    this.props.keysModel.signText(this.state.text).then(signature => {
      const newState = update(this.state,{signature:{$set:signature.toString()}})
      this.setState(newState);
    })
  }

  render() {
    return (
      <div>
        <span> Sign </span>
        <div className="row">
          <div className="col-sm-3">
            <ListKeysCmp keys={this.props.keysModel.keys} 
              selectKey={this.props.keysModel.selectKey} />
          </div>
          <div className="col-sm-9">
            <div>
            <span><label>Selected:</label> {this.props.keysModel.selectedKey.username}</span>              
              <div>
                <label>Signature:</label>
              </div>
              <textarea style={{height:120, width:570}} value={this.state.signature}></textarea> 
            </div>

            <div>
              <div>
                <label>Text:</label>  <button onClick={this.signText}> sign </button>
              </div>
              <textarea style={{height:230, width:570}} valueLink={this.linkState('text')} />
            </div>
          </div>
        </div>
      </div>
    );
  }

};

reactMixin(SignCtrl.prototype, LinkedStateMixin)

export default SignCtrl;
