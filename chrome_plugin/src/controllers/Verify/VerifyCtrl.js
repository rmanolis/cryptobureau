import React, {Component} from 'react';
import {observer} from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import EcdsaSrv from '../../services/EcdsaSrv.js';

import ListKeysCmp from '../../components/ListKeys/ListKeysCmp.js'

const CONTACTS_TYPE = "contacts";
const KEYS_TYPE = "keys";

@observer
class VerifyCtrl extends Component {
  constructor(props){
    super(props); 
    this.state = {
      text:"",
      publicKey : "",
      signature :"",
      isVerified:false,
      listType: CONTACTS_TYPE
    }
  }

  componentWillMount(){
    this.props.contactsModel.getAll();
    this.props.keysModel.getAll();
  }

  selectKey = (id) => {
    console.log("selected key", id);
    if(this.state.listType == CONTACTS_TYPE){
      this.props.contactsModel.getId(id).then(key =>{
        const newState = update(this.state,{publicKey:{$set:key.publicKey.toString()}})
        this.setState(newState);
      })
    }else if(this.state.listType == KEYS_TYPE){
      this.props.keysModel.getId(id).then(key =>{
        const newState = update(this.state,{publicKey:{$set:key.publicKey.toString()}})
        this.setState(newState);
      })
    }
  }

  chooseList = () =>{
    let lists = null;
    if(this.state.listType == CONTACTS_TYPE){
      lists = <ListKeysCmp keys={this.props.contactsModel.contacts} 
        selectKey={this.selectKey} />
    }else if(this.state.listType == KEYS_TYPE){
      lists = <ListKeysCmp keys={this.props.keysModel.keys} 
        selectKey={this.selectKey} />
    }
    return (
      <div>
        <div>
          <select valueLink={this.linkState('listType')}>
            <option value={CONTACTS_TYPE}>Contacts</option>
            <option value={KEYS_TYPE}>My keys</option>
          </select>
        </div>
        <div>
          { lists }
        </div>
      </div>
    )
  }

  verify = () =>{
    const {publicKey, signature, text} = this.state;
    console.log(this.state);
    EcdsaSrv.verify(publicKey, signature, text).then(isCorrect =>{
      const newState = update(this.state,{isVerified:{$set:isCorrect}})
      this.setState(newState);
    }).catch(error=>{
      console.log(error);
    })
  }

  verifyText=() =>{
    return (
      <div className="row">
        <div className="col-sm-4">
          <div className="row">
            <div>
              <label>Signature: </label>
            </div>
            <textarea style={{height:200, width:190}} valueLink={this.linkState('signature')}></textarea>
          </div>
          <div className="row">
            <div>
              <label>Public key: </label>
            </div>
            <textarea style={{height:200, width:190}}  valueLink={this.linkState('publicKey')}></textarea>
          </div>

        </div>
        <div className="col-sm-8">
          <div className="row">
            <div className="col-sm-6">
              <label>Verified: </label> 
              <span> {this.state.isVerified.toString()}</span>
            </div>
            <div className="col-sm-6">
              <button onClick={this.verify}>Verify</button>
            </div>
          </div>
          <div className="row">
            <div>
              <label>Text: </label>
            </div>
            <textarea style={{height:400, width:380}}  valueLink={this.linkState('text')}></textarea>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="row">
        <div className="col-sm-3">
          { this.chooseList() }
        </div>
        <div className="col-sm-9">
          { this.verifyText() }
        </div>
      </div>
    );
  }

};

reactMixin(VerifyCtrl.prototype, LinkedStateMixin)

export default VerifyCtrl;
