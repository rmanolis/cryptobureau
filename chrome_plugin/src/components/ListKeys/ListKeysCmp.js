import React, {Component} from 'react';
import {observer} from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import _ from 'lodash';

//props.keys { id, name, url }
//props.selectKey(id)
@observer
class ListKeysCmp extends Component {
  constructor(props){
    super(props); 
    this.state = {
      search : ""
    }
  }


  listNames(){
    const keys = _.filter(this.props.keys, key => {
      return key.username.startsWith(this.state.search);       
    });
    return (
      <div>
        { keys.map(x => 
                   <div className="row" key={x.id} >
                     <div className="col-sm-8">
                       <span onClick={() => this.props.selectKey(x.id)}>- { x.username }</span>
                     </div>
                   </div>
                   ) }
                 </div>
    )
  }

  searchName(){
    return (
      <div>
        <input type="text" valueLink={this.linkState('search')} />        
      </div>
    )
  }

  render() {
    return (
      <div>
        <div>
          {this.searchName()}
        </div>
        <div>
          {this.listNames()}
        </div>
      </div>
    )
  }
}

reactMixin(ListKeysCmp.prototype, LinkedStateMixin)

export default ListKeysCmp;

