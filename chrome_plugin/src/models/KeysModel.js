import {observable, computed} from 'mobx';
import EcdsaSrv from '../services/EcdsaSrv.js';
import KeysTableSrv from '../services/KeysTableSrv.js';

const KeysModel =  new class KeysStore {
  @observable keys = [];
  @observable selectedKey = {};


  getAll(){
    KeysTableSrv.getAll().then(function(data){
      console.log(data)
      this.keys = data;
    }.bind(this))
  }

  getId(id){
    return KeysTableSrv.get(id)
  }

  selectKey = (id)=>{
    KeysTableSrv.get(id).then(data=>{
      this.selectedKey = data;
    })
  }

  signText(text){
    return EcdsaSrv.sign(this.selectedKey.privateKey, text)
  }

  generateKey(username, url, email, curve){
    const self = this;
    EcdsaSrv.generateKey(curve).then(function(key){
      console.log( key, key.privateKey.toString());
      key.username = username;
      key.url = url;
      key.email = email;
      key.curve = curve;
      KeysTableSrv.add(key).then(function(id){
        console.log(id);
        KeysTableSrv.get(id).then(function(data){
          console.log(data)
          self.keys.push(data);
        }.bind(this))
      })
    })
  }

  deleteKey(id){
    KeysTableSrv.delete(id).then(data => {
      console.log("deleting key", data)
      this.getAll();
    }).catch(function(error){
      console.log(error)
    })
  }

}();

export default KeysModel;
