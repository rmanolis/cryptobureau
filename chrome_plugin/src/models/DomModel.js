import {observable, computed} from 'mobx';


const DomModel =  new class DomStore {
  @observable textToSign = "";
  @observable textToVerify = "";
  @observable signature  ="";

}();

export default DomModel;
