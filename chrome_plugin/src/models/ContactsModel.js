import {observable, computed} from 'mobx';
import EcdsaSrv from '../services/EcdsaSrv.js';
import ContactsTableSrv from '../services/ContactsTableSrv.js';

const ContactsModel =  new class ContactStore {
  @observable contacts = [];
  @observable selectedContact = {};


  getAll(){
    ContactsTableSrv.getAll().then(function(data){
      console.log(data)
      this.contacts = data;
    }.bind(this))
  }

  getId(id){
    return ContactsTableSrv.get(id)
  }

  selectContact = (id)=>{
    ContactsTableSrv.get(id).then(data=>{
      this.selectedContact = data;
    })
  }

  deleteContact =(id) =>{
    ContactsTableSrv.delete(id).then(() => {
      this.getAll();
    }).catch(function(error){
      console.log(error)
    })
  }

  addContact(username, url, email, publicKey){
    const contact = {
      username,
      url,
      email,
      publicKey,
    }
    ContactsTableSrv.add(contact).then(id => {
      console.log(id);
      ContactsTableSrv.get(id).then(data => {
        console.log(data)
        this.contacts.push(data);
      })
    })
  }

}();

export default ContactsModel;
