import React from 'react';
import ReactDOM from 'react-dom';
import {AppCtrl}  from './AppCtrl.js';
import AppModel from './models/AppModel';
import DomModel  from './models/DomModel';

chrome.tabs.getSelected(null, function(tab) {
  chrome.tabs.sendRequest(tab.id, {action: "getInformations"}, function(response) {
    console.log(response.textToSign);
    DomModel.textToSign = response.textToSign;
  });
});

ReactDOM.render(<AppCtrl appModel={AppModel} />, document.getElementById('root'));
