import React from 'react';
import {Router} from 'director';
import {when} from "mobx";
import {observer} from "mobx-react";
//controllers
import KeysCtrl from './controllers/Keys/KeysCtrl';
import ContactsCtrl from './controllers/Contacts/ContactsCtrl';
import SignCtrl from './controllers/Sign/SignCtrl';
import VerifyCtrl from './controllers/Verify/VerifyCtrl';
import HelpCtrl from './controllers/Help/HelpCtrl';

//models
import DomModel  from './models/DomModel';
import KeysModel from './models/KeysModel.js';
import ContactsModel from './models/ContactsModel.js';

@observer
class NavMenu extends React.Component{
  pages = [
    { url:'/keys', title:'Keys'},    
    { url:'/sign', title:'Sign'},
    { url:'/verify', title:'Verify'},
    { url:'/contacts', title:'Contacts'},  
    { url:'/', title:'Help'},    
  ];

  render(){
    console.log(this.pages);
    return (
      <div className="row">
          <span className="col-sm-1"></span>        
        { 
        this.pages.map(page => {
          return <span className="col-sm-2" key={page.url} ><a  onClick={()=> this.props.appModel.changePage(page.url)} > {page.title} </a></span>
        })}
      </div>
    )
  }
}


@observer
export class AppCtrl extends React.Component {

  constructor(props) {
    super(props);
  }

  getPage = () => {
    switch (this.props.appModel.page) {
      case "/":
        return <HelpCtrl />
      case "/keys":
        return <KeysCtrl keysModel={KeysModel} domModel={DomModel}/>
      case "/sign":
        return <SignCtrl keysModel={KeysModel} domModel={DomModel}/>
      case "/contacts":
        return <ContactsCtrl contactsModel={ContactsModel} domModel={DomModel} />
      case "/verify":
        return <VerifyCtrl contactsModel={ContactsModel} keysModel={KeysModel} domModel={DomModel}  />
    }
  }

  render() {
    const Children = this.props.appModel.component;
    return (
      <div className="container-fluid">
        <div className="appContent">
          <div>
            <NavMenu appModel={this.props.appModel}/>
          </div>
          { 
          this.getPage()
          }
        </div>
      </div>
    );
}

}


