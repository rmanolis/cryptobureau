import uuid from 'node-uuid';
import _ from 'lodash';
export class Key{
  constructor(keyBytes){
    this.key = keyBytes
  }
  getKey(){
    return this.key;
  }
  toString(){
    return  String.fromCharCode.apply(null,this.key);
  }
}


export class DB{
  constructor(table, transGet, transSet){
    this.table = table;
    this.transGet = transGet;
    this.transSet = transSet;
  }

  getAll(){
    const {table, transGet} = this;        
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {
        if (!chrome.runtime.error) {
          console.log(data);          
          if(!data[table]){
            reject("no data")
            return
          }
          let keys = [];
          _.forOwn(data[table], function(value, key) { 
            keys.push(transGet(key,value))
          } );
          resolve(keys);
        }else{
          reject(chrome.runtime.error)
        }
      });
    })
  }

  add(values){
    const {table, transSet} = this;
    console.log(table);
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {
        if (!chrome.runtime.error) {
          let keys = data[table];
          if(!keys){
            keys = {};
          }
          console.log(keys);
          const id = uuid.v4();
          keys[id] = transSet(values);
          data[table] = keys;
          console.log(data);
          chrome.storage.local.set(data, function() {
            if (chrome.runtime.error) {
              reject(chrome.runtime.error);
            }else{
              resolve(id);
            }
          });
        }else{
          reject(chrome.runtime.error)
        }
      });
    })
  }

  get(id){
    const {table, transGet} = this;    
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {

        if (!chrome.runtime.error) {
          if(!data[table]){
            reject("no data")
            return
          }
          const keys = data[table];
          console.log(keys);
          if(keys[id]){
            const value = keys[id];
            resolve(transGet(id, value))
          }else{
            reject("no data");
          }
        }else{
          reject(chrome.runtime.error)
        }
      });

    });
  }

  delete(id){
    const {table} = this;  
    console.log("deleting from ", table)
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {
        if (!chrome.runtime.error) {
          if(!data[table]){
            reject("no data")
            return
          }

          let keys = data[table];
          console.log("keys ",keys);
          if(keys[id]){
            console.log("deleting key", keys[id]);
            delete keys[id];
            const newKeys = _.omit(keys,id);
            console.log("new keys", newKeys);
            data[table] = newKeys;
            chrome.storage.local.set(data, function() {
              if (chrome.runtime.error) {
                reject(chrome.runtime.error);
              }else{
                resolve();
              }
            });
          }else{
            reject("no data");
          }
        }else{
          reject(chrome.runtime.error)
        }
      });

    });
  }

}
