import { Key } from './DBCreatorSrv.js';

const ecdsa = window.ecdsa.New();


function transformKey(ecdsaKey){
  const objKey = { 
    privateKey: new Key(ecdsaKey.PrivateKey), 
    publicKey: new Key(ecdsaKey.PublicKey),
  }
  return objKey
}

function isString(s) {
  return typeof(s) === 'string' || s instanceof String;
}


function toBytes(str) {
    var strUtf8 = unescape(encodeURIComponent(str));
    var ab = new Uint8Array(strUtf8.length);
    for (var i = 0; i < strUtf8.length; i++) {
        ab[i] = strUtf8.charCodeAt(i);
    }
    return ab;
}

const obj = {
  generateKey : function(curve){
    return new Promise(function(resolve, reject){
      const [key, error] = ecdsa.GenerateKeys(curve)
      if(error){
        reject(error);
      }else{
        const pkey =  transformKey(key);
        resolve(pkey);
      }
    });
  },
  sign: function(privateKey, text){
    return new Promise(function(resolve, reject){
      const [signature, error] = ecdsa.Sign(text, privateKey.getKey())
      if(error){
        reject(error);
      }else{
        const sign =  new Key(signature);
        resolve(sign);
      }
    });
  },
  verify: function(publicKey, signature, text ){
    if(isString(publicKey) && isString(signature)){
      console.log("verify public key", publicKey)
      
      return new Promise(function(resolve, reject){
        const [isCorrect, error] = ecdsa.Verify(toBytes(publicKey), text, toBytes(signature))
        if(error){
          reject(error);
        }else{
          resolve(isCorrect);
        }
      });
    }else{
      return new Promise(function(resolve, reject){
        const [isCorrect, error] = ecdsa.Verify(publicKey.getKey(),text, signature.getKey())
        if(error){
          reject(error);
        }else{
          resolve(isCorrect);
        }
      });
    }
  }

}


export default obj;
