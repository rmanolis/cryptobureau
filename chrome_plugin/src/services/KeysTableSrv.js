import { Key, DB } from './DBCreatorSrv.js';

function strToBytes(str){
  let uint=new Uint8Array(str.length);
  for(var i=0,j=str.length;i<j;++i){
    uint[i]=str.charCodeAt(i);
  }
  return uint
}

function transGet(id,data){
  return { 
    id, 
    privateKey: new Key(strToBytes(data.privateKey)),
    publicKey : new Key(strToBytes(data.publicKey)),
    username: data.username,
    email: data.email,
    url: data.url,
    curve: data.curve,
  }
}

function transSet(data){
  return { 
    privateKey: data.privateKey.toString(),
    publicKey : data.publicKey.toString(),
    username: data.username,
    email: data.email,
    url: data.url,
    curve: data.curve,
  }
}

const obj = new DB("keys", transGet, transSet ); 

export default obj
