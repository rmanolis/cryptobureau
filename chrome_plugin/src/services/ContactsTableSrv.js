import { Key, DB } from './DBCreatorSrv.js';

function strToBytes(str){
  let uint=new Uint8Array(str.length);
  for(var i=0,j=str.length;i<j;++i){
    uint[i]=str.charCodeAt(i);
  }
  return uint
}

function transGet(id,data){
  return { 
    id, 
    publicKey : new Key(strToBytes(data.publicKey)),
    username: data.username,
    email: data.email,
    url: data.url,
  }
}

function transSet(data){
  return { 
    publicKey : data.publicKey.toString(),
    username: data.username,
    email: data.email,
    url: data.url,
  }
}

const obj = new DB("contacts", transGet, transSet ); 

export default obj
