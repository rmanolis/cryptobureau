package models

import (
	"bitbucket.org/rmanolis/cryptobureau/utils"
	"bitbucket.org/rmanolis/cryptobureau/utils/ecdsa"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const LOGIN = "login"

type Login struct {
	Id         bson.ObjectId `bson:"_id,omitempty"`
	UserId     bson.ObjectId `bson:",omitempty"`
	Signature  string
	Text       string
	IP         string
	IsAccepted bool
	Date       time.Time
}

func GetLoginById(db *mgo.Database, id bson.ObjectId) (*Login, error) {
	l := new(Login)
	err := db.C(LOGIN).FindId(id).One(&l)
	return l, err
}

func (a *Login) Insert(db *mgo.Database) error {
	c := db.C(LOGIN)
	a.Id = bson.NewObjectId()
	a.Text = utils.RandSeq(100)
	a.Date = time.Now()
	return c.Insert(a)
}

func (a *Login) Update(db *mgo.Database) error {
	c := db.C(LOGIN)
	return c.UpdateId(a.Id, a)
}

func (a *Login) Remove(db *mgo.Database) error {
	c := db.C(LOGIN)
	return c.RemoveId(a.Id)
}

func (a *Login) Verify(u *User) (bool, error) {
	return ecdsa.Verify([]byte(u.PublicKey), a.Text, []byte(a.Signature))
}
