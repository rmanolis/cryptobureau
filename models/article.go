package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const ARTICLE = "article"

type Article struct {
	Id        bson.ObjectId ` bson:"_id,omitempty"`
	UserId    bson.ObjectId
	Title     string
	Text      string
	Date      time.Time
	Signature string
}

func (a *Article) Insert(db *mgo.Database) error {
	c := db.C(ARTICLE)
	a.Id = bson.NewObjectId()
	a.Date = time.Now()
	return c.Insert(a)
}

func (a *Article) Update(db *mgo.Database) error {
	c := db.C(ARTICLE)
	return c.UpdateId(a.Id, a)
}

func (a *Article) Remove(db *mgo.Database) error {
	c := db.C(ARTICLE)
	return c.RemoveId(a.Id)
}
