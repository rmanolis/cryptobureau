package models

import (
	"crypto/md5"
	"encoding/hex"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io"
)

const USER = "user"

type User struct {
	Id        bson.ObjectId `bson:"_id,omitempty"`
	Username  string
	Email     string
	Password  string
	PublicKey string
	IsAdmin   bool
}

func GetUserById(db *mgo.Database, id bson.ObjectId) (*User, error) {
	user := new(User)
	err := db.C(USER).FindId(id).One(&user)
	return user, err
}

func GetUserByName(db *mgo.Database, name string) (*User, error) {
	user := new(User)
	err := db.C(USER).Find(bson.M{"username": name}).One(&user)
	return user, err
}

func (u *User) Insert(db *mgo.Database) error {
	c := db.C(USER)
	u.Id = bson.NewObjectId()
	h := md5.New()
	io.WriteString(h, u.Password)
	u.Password = hex.EncodeToString(h.Sum(nil))
	return c.Insert(u)
}

func (u *User) Update(db *mgo.Database) error {
	c := db.C(USER)
	return c.UpdateId(u.Id, u)
}
