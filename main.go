package main

import (
	"crypto/elliptic"
	"encoding/gob"
	"flag"
	"net/http"

	"bitbucket.org/rmanolis/cryptobureau/confs"
	"bitbucket.org/rmanolis/cryptobureau/ctrls/routes"
	"bitbucket.org/rmanolis/cryptobureau/utils"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func main() {
	gob.Register(elliptic.P256())
	router := mux.NewRouter()
	routes.SetRouter(router)
	gdt := utils.NewDataStore(confs.DBName, "127.0.0.1", "27017")
	static := negroni.NewStatic(http.Dir("ui"))
	static.Prefix = ""
	n := negroni.New(negroni.NewRecovery(), confs.NewLogger(), static)
	n.Use(negroni.HandlerFunc(gdt.MgoMiddleware))
	opts := cors.Options{
		AllowedOrigins:   []string{"*"},
		ExposedHeaders:   []string{"*"},
		AllowedHeaders:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
		AllowCredentials: true,
	}

	handler := cors.New(opts).Handler(router)
	n.UseHandler(handler)
	devPtr := flag.Bool("deploy", false, "is for deployment")
	flag.Parse()
	if *devPtr == false {
		n.Run(":8001")
	} else {
		n.Run(":80")
	}
}
