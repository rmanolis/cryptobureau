import { browserHistory } from 'react-router'
import {observable, computed, autorun} from 'mobx';
import AuthSrv from '../services/AuthSrv.js';

const AuthState =  new class AuthStore {
  @observable user = { 
    id:null, 
    username:null,
    publicKey: null,
    email:null,
  };
  

  @observable loginId = "" ;
  @observable loginText = "";

  @computed get pages(){
    console.log("computed", this.user.username);
    let pages = [{ url:"/",text:"Home"},{ url:"/login",text:"Login"},{ url:"/register",text:"Register"}]
    if(this.user.username){
      pages = [{ url:"/",text:"Home"},{ url:"/logout",text:"Logout"}];
    }
    return pages;
  }

  constructor() {
    console.log("auth state");
    autorun(() => console.log("autorun", this.user.username))
    
  }
  
  currentUser(){
    AuthSrv.currentUser().then(function({data}){
      console.log("current user", data)
      this.user={ id : data.Id, username:data.Username};
      console.log(this.user.username);
    }.bind(this)).catch(function(){
      this.user = {};
    }.bind(this))
  }
  
  logout(cb){
     AuthSrv.logout().then(function(){
       this.user = {id:null}; 
     }.bind(this))
  }

  register(username, email, pubkey){
    AuthSrv.register(username, email, pubkey).then(function({data}){
      browserHistory.push('/')
    }.bind(this))
  }

  login(username, signature){
    AuthSrv.login(username,this.loginId, signature).then(function({data}){
      this.user = {id: data.Id,username : data.Username};
      browserHistory.push('/')      
    }.bind(this))
  }

  requestLogin(){
    AuthSrv.requestLogin().then(function({data}){
      this.loginId = data.Id;
      this.loginText = data.Text;
    }.bind(this))
  }
  
}();

export default AuthState;
