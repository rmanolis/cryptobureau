import React, { Component, PropTypes } from 'react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import {observer} from 'mobx-react';
import AuthState from '../../states/AuthState.js';

@observer
export class LoginCtrl extends Component{
  constructor(props) {
    super(props);
    this.state = {
      username:"",
      signature:"",
    }
  }

  componentWillMount(){
    this.props.authState.requestLogin();
  }


  login = ()=>{
    const {username, signature } = this.state;
    this.props.authState.login(username, signature);
  }

  render() {
    return (
      <div>
        <div><label>LoginId</label>
          <span> {this.props.authState.loginId} </span>
        </div>
        <div><label>Text </label>
          <span id="cb-text-sign">{this.props.authState.loginText}</span>
        </div>
         <div><span>Username</span>
          <input type="text" valueLink={this.linkState('username')} />
        </div>
        <div><span>Signature</span>
          <textarea id="cb-signature" valueLink={this.linkState('signature')} />
        </div>
        <button onClick={ this.login }> Login </button>
      </div>
    );
  }
}

reactMixin(LoginCtrl.prototype, LinkedStateMixin)

@observer
export class LoginPage extends Component {
  render() {
    return <LoginCtrl authState={AuthState} params={this.props.params}/>
  }
}

