import React, { Component, PropTypes } from 'react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import {observer} from 'mobx-react';
import AuthState from '../../states/AuthState.js';


@observer
export class RegisterCtrl extends Component{
  constructor(props) {
    super(props);
    this.state = {
      username:"",
      email: "",
      pubkey: "",
    }
  }
  componentDidUpdate(){
    if(this.props.error){
      alert(this.props.error);
    }

  }
  register = ()=>{
    const {username, email, pubkey } = this.state;
    this.props.authState.register(username,email,pubkey);
  }

  render() {
    
    return (
      <div>
        <div><span>Username</span>
          <input type="text" valueLink={this.linkState('username')} />
        </div>
        <div><span>Email</span>
          <input type="text" valueLink={this.linkState('email')} />
        </div>
        <div><span>Public key</span>
          <textarea valueLink={this.linkState('pubkey')} />
        </div>
        <button onClick={ this.register }> new </button>
      </div>
    );
  }
}

reactMixin(RegisterCtrl.prototype, LinkedStateMixin)

@observer
export class RegisterPage extends Component {
  render() {
    return <RegisterCtrl authState={AuthState} params={this.props.params}/>
  }
}


