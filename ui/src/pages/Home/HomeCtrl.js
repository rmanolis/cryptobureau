import React, {Component} from 'react';
import {observer} from 'mobx-react';
import HomeState from '../../states/HomeState.js';
import AuthState from '../../states/AuthState.js';


@observer
export class HomeCtrl extends Component {
  constructor(props){
    super(props);        
  }

  
  render() {
    return (
      <div>
        <span>User is { this.props.authState.user.username || "anonymous"} </span>
        <button onClick={this.onReset}>
          Press {this.props.homeState.timer }
        </button>
      </div>
    );
  }

  onReset = () => {
    this.props.homeState.timer += 1;
  }
};



@observer
export class HomePage extends Component {
  render() {
    return <HomeCtrl homeState={HomeState} authState={AuthState} params={this.props.params}/>
  }
}
