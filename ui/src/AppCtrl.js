import React from 'react';
import {Router} from 'director';
import {when} from "mobx";
import {observer} from "mobx-react";
import { Link } from 'react-router';
import AuthState from './states/AuthState.js';

@observer
export class AppCtrl extends React.Component {

  constructor(props) {
    super(props);
    
  }

  componentWillMount(){
    this.props.authState.currentUser();
  }

  renderNavMenu = observer(()=>{
    return(
      <div>
      <ul>
      {
        this.props.authState.pages.map(page => (<li key={page.text}>
          <Link to={page.url} >{page.text}</Link>
        </li>))
      }
      </ul>
      </div>
    )
  })

  render() {
    const { user } = this.props;
    const Nav = this.renderNavMenu
    return (
      <div className="container-fluid">
        <div className="appContent">
          <div>
            <Nav/>
          </div>
          {this.props.children}
          </div>
        </div>
    );
  }

}

@observer
export class AppPage extends React.Component {
  render() {
    return <AppCtrl authState={AuthState} params={this.props.params} children={this.props.children}/>
  }
}
