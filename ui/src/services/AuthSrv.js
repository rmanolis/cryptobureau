import axios from 'axios';

const AuthSrv = {
  register: function(username, email, pubkey){
    return axios.post('/auth/register',{
      Username:username,
      PublicKey:pubkey,
      Email:email
    });
  },

  login: function(username,loginId, signature){
    return axios.post(`/auth/login/${loginId}`, {
      Username:username,
      Signature:signature,
    });
  },

  requestLogin: function(){
    return axios.get("/auth/login");
  },

  currentUser: function(){
    return axios.get("/auth/user");
  },

  logout: function(){
    return axios.post("/auth/logout");
  },
}

export default AuthSrv;
