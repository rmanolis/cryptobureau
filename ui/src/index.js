import React from 'react';
import ReactDOM from 'react-dom';
import {IndexRoute, Router, Route, browserHistory} from 'react-router';
import AuthState from './states/AuthState.js';
import {AppPage}  from './AppCtrl.js';
import {HomePage} from './pages/Home/HomeCtrl.js';
import {LoginPage} from './pages/Login/LoginCtrl.js';
import {RegisterPage} from './pages/Register/RegisterCtrl.js';

function onLogout(next, replace){
  AuthState.logout()
  replace("/")
}

ReactDOM.render(<Router history={browserHistory}>
    <Route path='/' component={AppPage}  >
      <IndexRoute component={HomePage}/>
       <Route path="/register" component={RegisterPage}/>
       <Route path="/login" component={LoginPage}  />
       <Route path="/logout" onEnter={onLogout}/>
    </Route>
  </Router>, document.getElementById('root'));
