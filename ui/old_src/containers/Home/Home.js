import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {newLogin} from '../../actions/auth.js'

class Home extends Component{
  constructor(props) {
    super(props);
  }

 

  getNewLogin = ()=>{
    this.props.dispatch(newLogin());
  }
  checkUser = () =>{
    if(this.props.user.id){
      return <span> You are {this.props.user.username} </span>;
    }else{
      return <span> You are not user </span>;
    }
  }
  render() {
    console.log(this.props);
    return (
      <div>
        Home
        <br/>
        {  this.checkUser()      }
        <br/>
        <button onClick={ this.getNewLogin}> new </button>
      </div>
    );
  }
}

Home.propTypes = {
  dispatch: PropTypes.func.isRequired
};

Home.contextTypes= {
  router: React.PropTypes.object
}


const mapStateToProps = (state) => {
  const { auth } = state;
  return auth;
}

export default connect(
  mapStateToProps
)(Home);
