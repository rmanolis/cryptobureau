import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {login, newLogin, resetError} from '../../actions/auth.js'
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import { pushState } from 'react-router-redux'

class Login extends Component{
  constructor(props) {
    super(props);
    this.state = {
      username:"",
      signature:"",
    }
  }

  componentWillMount(){
    this.props.dispatch(newLogin());
  
  }


  login = ()=>{
    const {username, signature } = this.state;
    const {loginId} = this.props;
    this.props.dispatch(login(username,loginId,signature));
  }

  render() {
    return (
      <div>
        <div><label>LoginId</label>
          <span> {this.props.loginId} </span>
        </div>
        <div><label>Text</label>
          <span> {this.props.text} </span>
        </div>
         <div><span>Username</span>
          <input type="text" valueLink={this.linkState('username')} />
        </div>
        <div><span>Signature</span>
          <textarea valueLink={this.linkState('signature')} />
        </div>
        <button onClick={ this.login }> Login </button>
      </div>
    );
  }
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired
};
Login.contextTypes= {
  router: React.PropTypes.object
}

reactMixin(Login.prototype, LinkedStateMixin)
const mapStateToProps = (state) => {
  const { auth } = state;
  return auth;
}

export default connect(
  mapStateToProps
)(Login);
