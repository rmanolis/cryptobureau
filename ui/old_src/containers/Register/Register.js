import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {register, resetError} from '../../actions/auth.js'
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import { routeActions } from 'react-router-redux'


class Register extends Component{
  constructor(props) {
    super(props);
    this.state = {
      username:"",
      email: "",
      pubkey: "",
    }
  }
  componentDidUpdate(){
    if(this.props.error){
      alert(this.props.error);
      this.props.dispatch(resetError())
    }

  }
  register = ()=>{
    const {username, email, pubkey } = this.state;
    this.props.dispatch(register(username,email,pubkey));
    
  }

  render() {
    
    return (
      <div>
        <div><span>Username</span>
          <input type="text" valueLink={this.linkState('username')} />
        </div>
        <div><span>Email</span>
          <input type="text" valueLink={this.linkState('email')} />
        </div>
        <div><span>Public key</span>
          <textarea valueLink={this.linkState('pubkey')} />
        </div>
        <button onClick={ this.register }> new </button>
      </div>
    );
  }
}

Register.propTypes = {
  dispatch: PropTypes.func.isRequired
};
Register.contextTypes= {
  router: React.PropTypes.object
}

reactMixin(Register.prototype, LinkedStateMixin)
const mapStateToProps = (state) => {
  const { auth } = state;
  return auth;
}

export default connect(
  mapStateToProps
)(Register);
