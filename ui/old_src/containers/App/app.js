import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import {currentUser} from '../../actions/auth';

class App extends Component{
  constructor(props) {
    super(props);

  }
  componentWillMount(){
    this.props.dispatch(currentUser())
  }

  renderNavMenu =()=>{
    console.log(this.props);
    let pages=[{ url:"/",text:"Home"},{ url:"/login",text:"Login"},{ url:"/register",text:"Register"}];
    if(this.props.user.id){
      pages=[{ url:"/",text:"Home"},{ url:"/logout",text:"Logout"}];
    }
    return(
      <div>
        <ul>
        {
          pages.map(page => (<li key={page.text}>
            <Link to={page.url} >{page.text}</Link>
          </li>))
        }
        </ul>
      </div>
    )
  }

  render() {
    const { user } = this.props;
    const Nav = this.renderNavMenu
    return (
      <div className="container-fluid">
        <div className="appContent">
          <div>
            <Nav/>
            </div>
            {this.props.children}
            </div>
          </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.node.isRequired,
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired
};

App.contextTypes = {
  router: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  const { auth } = state;
  return {user: auth.user};
}

export default connect(
  mapStateToProps
)(App);
