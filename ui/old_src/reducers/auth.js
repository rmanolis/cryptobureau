import {NEW_LOGIN_REQUEST, NEW_LOGIN_SUCCESS, NEW_LOGIN_FAILURE,
  REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAILURE,
  LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE,
  CURRENT_USER_SUCCESS,CURRENT_USER_FAILURE,
  LOGOUT_SUCCESS, 
  RESET_ERROR
}  from '../actions/auth';


const initialState = {
  loginId: null,
  text: null,
  signature:null,
  user:{ 
    id:null,
    username:null
  },
  error:null,
}

export default function auth(state = initialState, action = {}){
  console.log(action);
  switch(action.type){
    case RESET_ERROR:
      return Object.assign({}, state, { error:null });
    case NEW_LOGIN_REQUEST:
      return state;
    case NEW_LOGIN_SUCCESS:
      return Object.assign({}, state, { loginId: action.payload.Id,text: action.payload.Text}); 
    case NEW_LOGIN_FAILURE:
      return Object.assign({}, state, {error: action.payload});
    case LOGIN_REQUEST:
      return state;
    case CURRENT_USER_SUCCESS:
      console.log("current user success")
      return Object.assign({}, state, {user:{id:action.payload.Id, username:action.payload.Username}});      
    case LOGIN_SUCCESS:
      return Object.assign({}, state, {user:{id:action.payload.Id, username:action.payload.Username}});
    case LOGIN_FAILURE:
      return Object.assign({}, state, {error: action.payload});
    case CURRENT_USER_FAILURE:
    case LOGOUT_SUCCESS:
      return Object.assign({}, state, {user:{id:null, username:null}});      
    case REGISTER_REQUEST:
    case REGISTER_SUCCESS:
      return state;
    case REGISTER_FAILURE:
      if(action.error){
        return Object.assign({}, state, {error: action.payload});        
      }
    default:
      return state
  }
}
