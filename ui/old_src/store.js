import { createStore, applyMiddleware, combineReducers, compose} from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import { apiMiddleware } from 'redux-api-middleware';
import { syncHistory, routeReducer } from 'react-router-redux'
import { browserHistory } from 'react-router'
import auth from './reducers/auth.js';

const logger = createLogger();
const reducer = combineReducers(
  { 
    auth,
    routing: routeReducer,
  }
);

const reduxRouterMiddleware = syncHistory(browserHistory)

const createStoreWithMiddleware = compose(
  applyMiddleware(thunk),
  applyMiddleware(logger),
  applyMiddleware(apiMiddleware),
  applyMiddleware(reduxRouterMiddleware),
  typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
)(createStore);

export default function configureStore(initialState){
  const store = createStoreWithMiddleware(reducer, initialState);
  reduxRouterMiddleware.listenForReplays(store)  
  return store
}
