import axios from 'axios';
import { routeActions } from 'react-router-redux'
import { createAction } from 'redux-actions';
import { CALL_API } from 'redux-api-middleware';

export const RESET_ERROR = 'RESET_ERROR';

export const CURRENT_USER_REQUEST = 'CURRENT_USER_REQUEST';
export const CURRENT_USER_SUCCESS = 'CURRENT_USER_SUCCESS';
export const CURRENT_USER_FAILURE = 'CURRENT_USER_FAILURE';

export const NEW_LOGIN_REQUEST = 'NEW_LOGIN_REQUEST';
export const NEW_LOGIN_SUCCESS = 'NEW_LOGIN_SUCCESS';
export const NEW_LOGIN_FAILURE = 'NEW_LOGIN_FAILURE';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';


export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';


export let resetError = function(){
  return createAction(RESET_ERROR)();
}

const registerAction = function(username, email, pubkey){
  return  {
    [CALL_API]:{
      endpoint: '/auth/register',
      method: 'POST',
      types: [REGISTER_REQUEST,  { 
        type: REGISTER_SUCCESS,
        payload:(action, state, res) => res.json()
      }, REGISTER_FAILURE],
      body:JSON.stringify({
        Username:username,
        PublicKey:pubkey,
        Email:email
      })  
    }
  }
}


export function register(username, email, pubkey){
  return dispatch =>{
    dispatch(registerAction(username, email, pubkey)).then(action => {
      if(!action.error){
        dispatch(routeActions.push('/'));  
      }
    })
  }
}


const loginAction = function(username,loginId, signature){
  return  {
    [CALL_API]:{
      endpoint: `/auth/login/${loginId}`,
      method: 'POST',
      credentials:'include',      
      types: [LOGIN_REQUEST, { 
        type: LOGIN_SUCCESS,
        payload:(action, state, res) => res.json()
      }, LOGIN_FAILURE],
      body:JSON.stringify({
        Username:username,
        Signature:signature,
      })  
    }
  }
}



export function login(username,loginId, signature){
  return dispatch =>{
    return dispatch(loginAction(username,loginId, signature)).then(action =>{
      if(action.error){
        alert(action.payload);
      }else{        
        dispatch(routeActions.push('/'));  
      }
    })
  }
}

const newLoginAction = function(){
  return  {
    [CALL_API]:{
      endpoint: '/auth/login',
      method: 'GET',
      types: [NEW_LOGIN_REQUEST, { 
        type:NEW_LOGIN_SUCCESS,
        payload:(action, state, res) => {
          return res.json()
        }
      }, NEW_LOGIN_FAILURE]
    }
  }
}


export function newLogin(){
  return dispatch =>{
    return dispatch(newLoginAction())
  }
}


const currentUserAction = function(){
  return  {
    [CALL_API]:{
      endpoint: '/auth/user',
      method: 'GET',
      credentials:'include',      
      types: [CURRENT_USER_REQUEST, { 
        type:CURRENT_USER_SUCCESS,
        payload:(action, state, res) => res.json()
      }, CURRENT_USER_FAILURE]
    }
  }
}

export function currentUser(){
  return dispatch =>{
    return dispatch(currentUserAction());
  }
}

const logoutAction = function(){
  return  {
    [CALL_API]:{
      endpoint: '/auth/logout',
      method: 'POST',
      credentials:'include',
      types: [LOGOUT_REQUEST, { 
        type:LOGOUT_SUCCESS,
        payload:(action, state, res) => res.json()
      }, LOGOUT_FAILURE]
    }
  }
}

export function logout(){
  return dispatch =>{
    return dispatch(logoutAction());
  }
}



