import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { routeActions } from 'react-router-redux'
import App from './containers/App/app.js';
import Home from './containers/Home/Home.js';
import Register from './containers/Register/Register.js';
import Login from './containers/Login/Login.js';
import {logout, currentUser} from './actions/auth.js';


import configureStore from './store';

const store = configureStore();
function onLogout(nextState, replaceState, cb) {
    store.dispatch(logout()).then(routeActions.push('/'))
}

function onCurrentUser(){
  store.dispatch(currentUser());
}


ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App} onEnter={onCurrentUser} >
        <IndexRoute component={Home}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
        <Route path="/logout" onEnter={onLogout}/>
      </Route>
    </Router>
  </Provider>
  ,document.getElementById('root')
);
