// cbcli is a command line tool to generate keys , create signatures and verify them
package main

import (
	"crypto/elliptic"
	"encoding/gob"
	"fmt"
	"io/ioutil"
	"os"

	"bitbucket.org/rmanolis/cryptobureau/utils/ecdsa"
	"github.com/codegangsta/cli"
)

func main() {
	gob.Register(elliptic.P256())
	gob.Register(ecdsa.Signature{})
	app := cli.NewApp()
	app.Name = "cbcli"
	app.Usage = "Create and verify digital signature"
	app.Commands = []cli.Command{generate, verify, sign}

	app.Run(os.Args)
}

// generate --filename=
var generate = cli.Command{
	Name:        "generate",
	Aliases:     []string{"g"},
	Usage:       "use it to generate public and private key",
	Description: "It genarate public and private key",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "filename",
			Value: "default_key",
			Usage: "filename",
		},
		cli.StringFlag{
			Name:  "curve",
			Value: "P256",
		},
	},
	Action: func(c *cli.Context) {
		curve_name := c.String("curve")
		keys, err := ecdsa.GenerateKeys(curve_name)
		if err != nil {
			fmt.Println("It didnt generate key")
		}
		ioutil.WriteFile(c.String("filename")+".cbpriv", keys.PrivateKey, 0644)
		ioutil.WriteFile(c.String("filename")+".cbpub", keys.PublicKey, 0644)
	},
}

// verify --public= --text= --signature=
var verify = cli.Command{
	Name:        "verify",
	Aliases:     []string{"v"},
	Usage:       "use it to verify",
	Description: "It verifies a text from its signature and public key",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "public",
			Usage: "public",
		},
		cli.StringFlag{
			Name:  "signature",
			Usage: "signature",
		},
		cli.StringFlag{
			Name:  "text",
			Usage: "text",
		},
	},
	Action: func(c *cli.Context) {
		pub, err := ioutil.ReadFile(c.String("public"))
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		text, err := ioutil.ReadFile(c.String("text"))
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		signature, err := ioutil.ReadFile(c.String("signature"))
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		v, err := ecdsa.Verify(pub, string(text), signature)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		fmt.Println(v)
	},
}

//signature --private= text=
var sign = cli.Command{
	Name:        "signature",
	Aliases:     []string{"s"},
	Usage:       "use it to sign a text",
	Description: "It genarates a signature from text and private key",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "private",
			Usage: "private",
		},
		cli.StringFlag{
			Name:  "text",
			Usage: "text",
		},
		cli.StringFlag{
			Name:  "file",
			Usage: "file",
		},
	},
	Action: func(c *cli.Context) {
		private, err := ioutil.ReadFile(c.String("private"))
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		text := []byte{}
		if len(c.String("text")) > 0 {
			text = []byte(c.String("text"))
		} else {
			text, err = ioutil.ReadFile(c.String("file"))
			if err != nil {
				fmt.Println(err.Error())
				return
			}
		}

		sign, err := ecdsa.Sign(string(text), private)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Println(string(sign))
		//ioutil.WriteFile("signature.cbsign", sign, 0644)
	},
}
